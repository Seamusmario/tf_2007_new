# Welcome to tf_2007_new
 An mod named "Civilian 2.5" which is basically Lambda Fortress and Garry's Mod smashed together.

## TODO

- [ ] Fix the match hud
- [ ] Add almost every MvM features 
- [x] Add Raid Mode entities and features.

## Disclaimer

This mod uses leaked code from the tf_port base & 2021 TF2 and CS:GO source code leak which is owned by Valve. I do not own any code in this mod, Most of it belongs to Valve itself.

The source code for npc_infected is from https://github.com/InfoSmart/InSource/blob/master/game/server/sdk/npc_infected.cpp. Credit goes to everyone.
