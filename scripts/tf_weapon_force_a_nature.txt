WeaponData
{
	// Attributes Base.
	"printname"				"#TF_Weapon_Scattergun"
	"BuiltRightHanded"		"0"
	"weight"				"3"
	"WeaponType"			"item2"
	"ITEM_FLAG_NOITEMPICKUP" 	"0"
	
	// Primary Attributes
	"Damage"				"8"
	"Range"					"8192"
	"BulletsPerShot"		"12"
	"Spread"				"0.0675"
	"PunchAngle"			"3.0"
	"TimeFireDelay"			"0.275"
	"TimeIdle"				"5.0"
	"TimeIdleEmpty"			"0.25"
	"TimeReloadStart"		"0.1"
	"TimeReload"			"0.5"
	"primary_ammo"			"TF_AMMO_PRIMARY"
	"clip_size"				"2"
	"default_clip"			"2"
	"ProjectileType"		"projectile_bullet"
	"AmmoPerShot"			"1"
	"HasTeamSkins"			"1"
	
	"DoInstantEjectBrass"	"0"
	"BrassModel"			"models/weapons/shells/shell_shotgun.mdl"	
	"TracerEffect"		"bullet_scattergun_tracer01"

	// Buckets.
	"bucket"				"0"
	"bucket_position"		"0"

	// Animation.
	"viewmodel"     	"models/weapons/v_models/v_double_barrel.mdl"
	"playermodel"		"models/weapons/c_models/c_double_barrel.mdl"
	"anim_prefix"		"shotgun"

	// Muzzleflash
	"MuzzleFlashParticleEffect" "muzzle_scattergun"
	
	// Sounds.
	// Max of 16 per category (ie. max 16 "single_shot" sounds).
	SoundData
	{
		"single_shot"		"Weapon_Scatter_Gun_Double.Single"
		"empty"			"Weapon_Scatter_Gun.Empty"
		"reload"			"Weapon_SMG.WorldReload"
		"special1"		"Weapon_Scatter_Gun.Pump"
		"burst"			"Weapon_Scatter_Gun_Double.SingleCrit"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"file"		"sprites/bucket_scatgun"
				"x"		"0"
				"y"		"0"
				"width"		"200"
				"height"		"128"
		}
		"weapon_s"
		{	
				"file"		"sprites/bucket_scatgun"
				"x"		"0"
				"y"		"0"
				"width"		"200"
				"height"		"128"
		}
		"ammo"
		{
				"file"		"sprites/a_icons1"
				"x"		"55"
				"y"		"60"
				"width"		"73"
				"height"	"15"
		}
		"crosshair"
		{
				"file"		"sprites/crosshairs"
				"x"		"0"
				"y"		"0"
				"width"		"32"
				"height"	"32"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"		"0"
				"y"		"48"
				"width"		"24"
				"height"	"24"
		}
	}
}