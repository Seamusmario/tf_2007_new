fWeaponData
{
	// Attributes Base.
	"printname"			"#TF_Weapon_Shotgun"
	"BuiltRightHanded"		"0"
	"weight"			"2"
	"WeaponType"			"primary"
	"ITEM_FLAG_NOITEMPICKUP" 	"0"
	
	// Primary Attributes
	"Damage"			"32"
	"Range"				"8192"
	"BulletsPerShot"		"10"
	"Spread"			"0.0675"
	"PunchAngle"			"2.0"
	"TimeFireDelay"			"1.0"
	"TimeIdle"			"5.0"
	"TimeIdleEmpty"			"0.25"
	"TimeReloadStart"		"0.1"
	"TimeReload"			"0.5"
	"primary_ammo"			"TF_AMMO_PRIMARY"
	"clip_size"			"8"
	"default_clip"			"8"
	"ProjectileType"		"projectile_bullet"
	"AmmoPerShot"			"1"
	"HasTeamSkins_Viewmodel"			"1"
	
	"DoInstantEjectBrass"	"0"
	"BrassModel"			"models/weapons/shells/shell_shotgun.mdl"	
	"TracerEffect"		"bullet_shotgun_tracer01"

	// Buckets.
	"bucket"			"0"
	"bucket_position"		"0"

	// Animation.
	"playermodel"		"models/weapons/w_models/w_shotgun.mdl"
	
	"viewmodel"			"models/weapons/v_models/v_shotgun_engineer.mdl"
	"anim_prefix"   "shotgun"

	// Muzzleflash
	"MuzzleFlashParticleEffect" "muzzle_shotgun"
	
	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	SoundData
	{
		"single_shot"		"Shotgun.Fire"
		"burst"		"Shotgun.Fire"
		"reload"		"Shotgun.WorldReloadShell"
		"reload_pump"		"Shotgun.WorldReloadPump"
		"shoot_incendiary"	"Shotgun.FireIncendiary"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"file"		"sprites/bucket_shotgun"
				"x"		"0"
				"y"		"0"
				"width"		"200"
				"height"		"128"
		}
		"weapon_s"
		{	
				"file"		"sprites/bucket_shotgun"
				"x"		"0"
				"y"		"0"
				"width"		"200"
				"height"		"128"
		}
		"ammo"
		{
				"file"		"sprites/a_icons1"
				"x"		"55"
				"y"		"60"
				"width"		"73"
				"height"	"15"
		}
		"crosshair"
		{
				"file"		"sprites/crosshairs"
				"x"		"0"
				"y"		"0"
				"width"		"32"
				"height"	"32"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"		"0"
				"y"		"48"
				"width"		"24"
				"height"	"24"
		}
	}
}